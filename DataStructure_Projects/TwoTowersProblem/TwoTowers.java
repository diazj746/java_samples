import structure5.*;
import java.util.Iterator;
/**
 * Lab 6: The Two-Towers Problem
 * Suppose that we are given n uniquely sized cubic blocks and 
 * that each block has a face area between 1 and n.
 * 
 * Find the set of blocks that is closest to half a tower.
 * 
 * @author Sean Atkinson, Andrew Cotton, Jorge Diaz 
 * @version 1.0
 */
public class TwoTowers
{
    /**
     * Write a main method that inserts values as double in a Vector. 
     * 
     * - Subset Iterator will construct 2^n subsets of 15 double 
     *   values (sqrt(1), sqrt(2), ... n).
     * - Sum the subsets and keep the one thats closests
     *   (but does not exceed)the Sum of the input Vector/2
     * - Print that solution.
     * - Time it for thought questions
     */
     public static void main(String args[])
     {
         //Make the testVector
         int testSize = 40;
         Double testSum = 0.0d;
         Double testTemp = 0.0d;
         Double height = 0.0d;
         Vector<Double> subSet;
         Vector<Double> solution = new Vector<Double>();
         Vector<Double> testVector = new Vector<Double>(testSize);
            
         //Add values
         for (int i=0 ; i<testSize; i++)
         {
             testTemp = Math.sqrt(i);
             testSum = testSum + testTemp;
             testVector.add(testTemp);
         }
         //Target Height
         height = testSum/2;
         
         SubsetIterator subsetVector = new SubsetIterator<Double,Vector<Double>>(testVector);
         
         int count = 0;
         
         //Here we will store temp values
         testTemp = 0.0d;
         //Test speed
         Timer clock = new Timer();
         clock.startTime();
         while (subsetVector.hasNext()) 
         {  
             //Get the subset
             subSet = subsetVector.next();
             
             //Zero out new Sum
             testSum = 0.0d;
             //Sum it's contents
             for(Double d : subSet)
             { 
                testSum = testSum + d; 
             }
             
             if( testSum <= height && testSum > testTemp)
             {
                 testTemp = testSum;
                 solution = subSet;
             }
             count++;
         }
         //Get Timer
         long finalTime = clock.getElapsedTime();
         //Stop Timer
         clock.stopTime();
         
         System.out.println("-------------------");
         System.out.println("Two Towers Solution");
         System.out.println("-------------------");
         System.out.println("Start Array = " + testVector);
         System.out.println("Best Solution = " + solution);
         System.out.println("Half height = " + height);
         System.out.println("Solution height = " + testTemp);
         System.out.println("Examined Total = " + count + " Sets");
         System.out.println("Total Miliseconds = " + finalTime);
    }
}
