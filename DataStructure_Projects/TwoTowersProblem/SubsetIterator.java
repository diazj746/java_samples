import structure5.*;
import java.util.Iterator;
/**
 * SubsetIterator
 * extends the structure5.* Abstract Iterator by Duane A. Bailey
 * 
 * This class takes in a Vector of values<E> and return all possible 
 * subsets as Vectors in its next() method.
 * 
 * Get returns the <E> value in stored in the Vector at the current 
 * 
 * @authors Sean Atkinson, Andrew Cotton, Jorge Diaz
 * @version 1.0
 */

/**
 * This class
 * - Uses object T that must extend Vector<E>
 * - T has to extend AbstractIterator of the same type T
 */
public  class SubsetIterator<E,T extends Vector<E>> extends AbstractIterator<T>
{
    //Our internal T
    protected Vector<E> myVector;
    /*The "Finger", is not the place we are in the current vector
     * but rather an int number from 0 to 2^n where n is the size of the vector.
     * Our finger is an index that represents a binary set.
     */
    protected int scanIndex;

    /**
     * Constructor takes in a single Vector of type <E>
     */
    public SubsetIterator(Vector<E> v)
    {
        //Initialize
        myVector = v;
        //Reset the finger
        reset();
    }
    
    /**
     * get() = AbstractIterator overwrite
     * Does not increment scanIndex
     * @return     T Vector<E> with subsets of the binary representation 
     *             of the current "finger" scanIndex
     */
    public T get()
    {
        return (T) generateSubset();
    }
    
    /**
     * next() = AbstractIterator overwrite
     * Increments scanIndex
     * @return     T Vector<E> with subsets of the binary representation 
     *             of the current "finger" scanIndex
     */  
    public T next()
    {
        //Increment Scan Index
        Vector<E> subset = generateSubset();
        scanIndex++;
        
        return (T) subset;
    }
    
    /**
     * hasNext() = AbstractIterator overwrite
     * According to Bailey there are exactly 2^n subsets of values 0 to n−1.
     * We have generated all the sets if our scanIndex >= 2^n the size of our array.
     * @return     boolean
     */    
    public boolean hasNext()
    {
        return  scanIndex < Math.pow(2,myVector.size());
    }
    
    /**
     * reset() = AbstractIterator overwrite
     * Sets scanIndex back to 0 so we can reuse instances of this class
     * @return     boolean
     */  
    public void reset()
    {
        scanIndex = 0;
    }
    
    /**
     * generateSubset() = private helper function
     * Uses the binary form of the value at position scanIndex (example 2 = 010)
     * to generate a subset the values in myVector.
     * 
     * These subsets are based on scanIndex binary representation
     * 
     * NOTE: This method always gives the following warning at compile time.
     * "SubsetIterator.java uses unchecked or unsafe operations."
     * Because it returns an array as an object and that can be trouble.
     * 
     * @return     T Vector<E> with all the subsets for the binary representation of "scanIndex"
     */    
    private T generateSubset()
    {
        //Our subset
        Vector<E> tempSubsetVector = new Vector<E>();
        
        for (int i = 0; i<myVector.size(); i++)
        {
            /* In Bailey we learned that:
             * - "2^n different sequences generate binary numbers in the range 0 through 2n − 1.
             * - "Count from 0 to 2n−1 and use the binary digits (bits) of the number to determine
             *   which of the original values of the Vector are to be included in a subset.
             * 
             * From right to left.
             * - 1L<<i = Creates a number with 63 zeros spaces and a 1 at position i that we use for masking.
             * - Use bitwise AND (&) to see if the bit at position i, in scanIndex is a 0 or a 1.
             * 
             * When the bit is a 1 we add it to the vector
             */
            if((scanIndex & (1L<<i))>0)
            {
                /* NOTE: 
                 * Here the add() and get() methods are those 
                 * of standard Vector Class for "tempSubsetVector"
                 * NOT those of our class SubsetIterator
                 */
                tempSubsetVector.add(myVector.get(i));
            }
        }
        return (T) tempSubsetVector;
    }
    
    /*
     * For testing: write a main method of your SubsetIterator that:
     * 
     * - creates a Vector<Integer> with the Integers from 0 through 7
     * - creates a SubsetIterator<Integer,Vector<Integer>> with this Vector<Integer>
     * - prints out all subsets returned and a count of the subsets returned
     * - Make sure you end up with 256 subsets printed. (2^8 = 256)
     * 
     * Please leave this main method in your program when you submit.
     */
     public static void main(String args[])
     {
         //Make the testVector
         int testSize = 8;
         Vector<Integer> testVector = new Vector<Integer>(testSize);
            
         //Add values 0-7
         for (int i=0 ; i<testSize; i++)
         {
             testVector.add(i);
         }
           
         System.out.println("Creating a subset Iterator:");
         SubsetIterator subsetVector = new SubsetIterator<Integer,Vector<Integer>>(testVector);
         int count = 0;
         while (subsetVector.hasNext()) 
         {  
             System.out.println(subsetVector.next());
             count++;
         }
         System.out.println("Total = " + count + " Sets");
    }  
}
