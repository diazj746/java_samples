
/**
 * Timmer is a small utility class for the Project 6 assigments.
 * 
 * @author Sean Atkinson, Jorge Diaz and Andrew Cotton 
 * @version Version 1.1 9/20/2013
 */
public class Timer
 {
     private long startTime;
     
    public Timer ()
    {
       startTime = 0;
    }
    /**
     * Sets the current nanoTime as the start time
     */
    public long startTime()
    {
        return startTime = System.nanoTime();
    }
    
    /**
     * Returns difference of start time and current time
     */
    public long getElapsedTime()
    {
        return (System.nanoTime() - startTime);
    }
    /**
     * Sets timer to 0.
     */
    public void stopTime()
    {
        startTime = 0;
    }
    
   public static void main(String[] args) 
   {
       int i , loops;
       loops = 100000;
       int total = 0;
       long finalTime = 0;
       Timer watch = new Timer();
       watch.startTime();
       
      for (i = 0; i < loops; i++) {
         total += i;
      }
      
      finalTime = watch.getElapsedTime();
      watch.stopTime();
      System.out.println("Elapsed time: "+ finalTime + " nanoseconds");   
    }
}