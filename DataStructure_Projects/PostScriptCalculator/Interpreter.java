import structure5.*;
import java.util.Iterator;
/**
 * Interpreter
 * 
 * Implements a PostScript-based calculator
 * using: Token.java,SymbolTable.java, Reader.java and structure.* lib
 * 
 * Supports the following PostScript commands:
 * - pstack 
 * - add 
 * - sub 
 * - mul 
 * - div
 * - dup 
 * - exch 
 * - eq 
 * - ne 
 * - def
 * - pop
 * - quit 
 * - ptable
 * Also implement
 * the nonstandard PostScript command ptable that prints the symbol table.
 * 
 * 1. If we are performing an eq operation, is it necessary to assume
 * that the values on the top of the stack are, say, numbers?
 * Answer: No, token already checks for type compatibility. If incompatible it will 
 * 
 * 2. What is the most elegant way of implementing "pstack"?
 * Answer: The most elegant way is to use an iterator since my stack
 * implements the iterator interface.  A not so clean way would be to
 * pop everything from our main stack(a) to another stack(b) then print
 * it as you pop it back(to a) but this is expensive O(2n) to do.
 * 
 * 5. What does the following do? 
 * /count { dup 1 ne { dup 1 sub count } if } def
 * 10 count pstack
 * Answer:
 * It's a recursive operation
 * that counts down and leaves the stack like this
 * 10 9 8 7 6 5 4 3 2 1
 *
 * count(int i)
 * {
 *  push(i);
 *  if (i != 1)
 *  {
 *     count(i - 1);
 *  }
 * }
 * 
 * @author Jorge Diaz
 * @version 1.0 CS 501 Fall 2013
 */
public class Interpreter
{
    // Used to make methods print during execution
    protected boolean debugMode = false; 
    protected structure5.StackList<Token> st;
    protected SymbolTable sm;

    /**
     * Interpreter
     * Constructor
     * @post creates an intepreter instance
     */
    public Interpreter()
    {
        this.st = new StackList<Token>();
    } 
    
    /**
     * Interpreter
     * Constructor override used in debug moode
     * @param  b   sets debug mode on = true, default is off
     */
    public Interpreter(boolean b)
    {
        if(b)
        {
            this.debugMode = b;
            System.out.println("New Interpreter created in Debug Mode");
        }
        this.st = new StackList<Token>();
    }
    
    /**
     * add
     * pops off previous two double values form our stack 
     * and adds them together. 
     * result is pushed back on the stack
     * @pre values are doubles
     * @post values are added
     */
    public void add()
    {   
        debugPrint("Running add()");
        Token a = st.pop();
        Token b = st.pop();
        if (a.isProcedure()){pushProcedure(a); interpret(st.pop()); a = st.pop();}
        if (b.isProcedure()){pushProcedure(b); interpret(st.pop()); b = st.pop();}
        Token c = new Token(a.getNumber()+ b.getNumber());
        debugPrint("add(): " 
                    + a.toString() + " + "
                    + a.toString() + " = " 
                    + c.toString());
        st.push(c);
    }
    
    /**
     * sub
     * pops off previous two values form our stack 
     * and substracts them together. 
     * result is pushed back on the stack
     * @pre values are doubles
     * @post values are added
     */
    public void sub()
    {
        debugPrint("Running sub()");
        Token a = st.pop();
        Token b = st.pop();
        if (a.isProcedure()){pushProcedure(a); interpret(st.pop()); a = st.pop();}
        if (b.isProcedure()){pushProcedure(b); interpret(st.pop()); b = st.pop();}
        Token c = new Token(a.getNumber()-b.getNumber());
        debugPrint("sub(): " 
                    + a.toString() + " - "
                    + a.toString() + " = " 
                    + c.toString());
        st.push(c);   
    }
    
    /**
     * mul
     * pops off previous two values form our stack 
     * first / next
     * and multiplicates them. 
     * result is pushed back on the stack
     */
    public void mul()
    {
        debugPrint("Running mul()");
        Token a = st.pop();
        Token b = st.pop();
        if (a.isProcedure()){pushProcedure(a); interpret(st.pop()); a = st.pop();}
        if (b.isProcedure()){pushProcedure(b); interpret(st.pop()); b = st.pop();}
        Token c = new Token(a.getNumber()*b.getNumber());
        debugPrint("mul(): " 
                    + a.toString() + " * "
                    + a.toString() + " = " 
                    + c.toString());
        st.push(c);          
    }
    
    /**
     * div
     * pops off previous two values form our stack 
     * and divides them.
     * division by zero throws exception
     * result is pushed back on the stack
     */
    public void div()
    {
        debugPrint("Running div()");
        Token a = st.pop();
        Token b = st.pop();
        if (a.isProcedure()){pushProcedure(a); interpret(st.pop()); a = st.pop();}
        if (b.isProcedure()){pushProcedure(b); interpret(st.pop()); b = st.pop();}
        if (b.getNumber()>0)
        {
            Token c = new Token(a.getNumber()/b.getNumber());
            debugPrint("sub(): " 
                        + a.toString() + " / "
                        + a.toString() + " = " 
                        + c.toString());
            st.push(c);  
        }
        else
        {
            System.out.println("div() Error: division by zero not supported");
            System.exit(1);
        }    
    }
    
    /**
     * dup
     * Duplicates previous value
     * and pushes it back on the stack
     * supports all token value types
     */
    public void dup()
    {
        debugPrint("Running dup()");
        Token a = st.peek();
        Token b = null;
        if (a.isNumber())
        { b = new Token(a.getNumber());}
        else if (a.isBoolean())
        { b = new Token(a.getBoolean());}
        else if (a.isSymbol())
        { b = new Token(a.getSymbol());}
        else if (a.isProcedure())
        { b = new Token(a.getProcedure());}
        st.push(b);
        debugPrint("dup(): " 
                    + a.toString());
    }
        
    /**
     * exch
     * exchanges the postion of last
     * 2 values in the stack
     */
    public void exch()
    {
        debugPrint("Running exch()");
        Token a = st.pop();
        Token b = st.pop();
        debugPrint("exch(): " 
                    + a.toString() + " and "
                    + b.toString());
        st.push(b);
        st.push(a);
    }
    
    /**
     * eq
     * Boolean operation
     * tests if equal value
     * pushes back true or false
     */
    /* If we are performing an eq operation, is 
     * it necessary to assume that the
     * values on the top of the stack are, 
     * say, numbers?
     */
    public void eq()
    {
        debugPrint("Running eq()");
        Token a = st.pop();
        Token b = st.pop();
        Token c = new Token(a.equals(b));
        debugPrint("eq(): " 
                    + a.toString() + " and "
                    + b.toString() + " is "
                    + c.toString());
        st.push(c);   
    }
    
    /**
     * ne
     * Boolean operation
     * tests if not-equal value
     * pushes back true or false
     */
    public void ne()
    {
        debugPrint("Running ne()");
        Token a = st.pop();
        Token b = st.pop();
        Token c = new Token(!a.equals(b));
        debugPrint("ne(): " 
                    + a.toString() + " and "
                    + b.toString() + " is "
                    + c.toString());
        st.push(c);    
    }
    
    /**
     * def
     * Defines or replaces an existing symbolic value 
     * of the same name in our symbols table.
     * value is specified as:
     * - "quoted" symbol preceded by /
     * - value association
     * - "def" trigger command
     * ex: /pi 3.141592653 def
     * @post - creates or updates symbol table
     * @post - pops the stack until it finds "/"
     * @post - adds a single values to symbol table multiple values are added as a procedure
     */
    public void def()
    {
        debugPrint("Running def()");
        //We'll save tokens here
        List<Token> tokens = new CircularList<Token>();
        
        if (st.size()>1)
        {    
            String defname = st.peek().toString().substring(0,1);
            
            do
            {
                debugPrint("exit clause = " + defname);
                if (!defname.equals("/"))//Look for the defname
                {
                    Token last = st.pop();
                    debugPrint("adding token = " + last.toString());
                    debugPrint("next token = " + st.peek().toString());
                    tokens.add(last);
                    if(st.size() > 0)
                    {
                        defname = st.peek().toString().substring(0,1);
                    }
                    else{ break;}
                }
            }
            while (!defname.equals("/") && st.size() > 0);
            
            if(st.size() > 0)
            {
                Token first = st.pop();
                String name = first.toString();
                debugPrint("final token = " + first.toString());
                debugPrint("final token name = " + first.toString());
                name = name.substring(1); //Remove "/"
                debugPrint("final token name Post = " + name);
                
                Token result;
                //Single Value
                if (tokens.size()== 1)
                {
                    result = tokens.removeFirst();
                }
                //Procedure
                else
                {
                    result = new Token(tokens);
                }
                
                debugPrint("Inserting def: " + name + "," + result.toString());
            
                if (sm == null){sm = new SymbolTable();}
                //Search
                if (sm.contains(name)){sm.remove(name);}
                //Add
                sm.add(name,result);
            }
            else
            {
                System.out.println("def() error could not find identifier");
            }
        }
    }
    
    /**
     * ptable
     * Nonstandard PostScript command
     * that prints the symbol table
     */
    public void ptable()
    {
        debugPrint("Running ptable()");
        if (sm != null)
        {
            System.out.println(sm.toString());
        }
        else
        {
            System.out.println("Symbol Table was never used");
        }
    }
    
    /**
     * pstack
     * Prints the contents of operand stack
     * without destroying it
     */
    public void pstack()
    {
        debugPrint("Running pstack()");
        
        Iterator<Token> i = st.iterator();
        String result = "";
        while (i.hasNext())
        {
            result = result + i.next().toString() + " ";
        }
        if (result.equals(""))
        {
           System.out.println("Empty pstack");
        }
        else
        {
            System.out.println(result);
        }
    }
    
    /**
     * interpret
     * Pushes the token to our interpreter
     * @param a token non null
     * @post token is interpreted
     */
    public void interpret(Token t)
    {    
        debugPrint("Running interpret() " + t.toString());
        
        if (t.isSymbol())
        {
            String s = t.toString();
            debugPrint("Evaluating Symbol \'" + s + "\'."); 
            switch(s)
            {
                case "pstack":  
                    pstack(); break;
                case "add":
                    if (st.size()>= 2){add();} 
                    else{System.out.println("\'interpret()\' did not find two values to add");}
                    break;
                case "sub":
                    if (st.size()>= 2){sub();}
                    else{System.out.println("\'interpret()\' did not find two values to substract");}
                    break;
                case "mul":
                    if (st.size()>= 2){mul();}
                    else{System.out.println("\'interpret()\' did not find two values to multiply");}
                    break;
                case "div":
                    if (st.size()>= 2){div();}
                    else{System.out.println("\'interpret()\' did not find two values to divide");}
                    break;
                case "dup":
                    if (st.size()>0){dup();}
                    else{System.out.println("\'interpret()\' did not find value to duplicate");}
                    break;
                case "exch":
                    if (st.size()>=2){exch();}
                    else{System.out.println("\'interpret()\' did not find two values to exchange");}
                    break;
                case "eq":
                    if (st.size()>=2){eq();}
                    else{System.out.println("\'interpret()\' did not find two values to equal");}
                    break;
                case "ne":
                    if (st.size()>=2){ne();}
                    else{System.out.println("\'interpret()\' did not find two values to not equal");}
                    break;
                case "pop":
                    pop();
                    break;
                case "ptable":
                    ptable();
                    break;
                default:
                    if(s.equals("def"))
                    {
                       debugPrint("interpret() default case 'def': (\'" + s + "\')");
                       def();
                    }
                    else 
                    {
                       //Check symbol table 
                       debugPrint("interpret() default case look for symbol in symbol table: (\'" + s + "\')");
                       if (sm != null)
                       {
                           //Look for symbol
                           if(sm.contains(s))
                           {
                               Token found = cloneToken(sm.get(s));//Lets clone and not use a ref
                               debugPrint("Success: symbol found, retrieving: (\'" + found.toString() + "\')");
                               st.push(found);
                           }
                           else
                           {
                               debugPrint("Fail: symbol not found pushing: (\'" + s + "\')"); 
                               st.push(t);
                           }
                       }
                       //Add the value
                       else
                       { 
                           debugPrint("Symbol Table not found adding: (\'" + s + "\')"); 
                           st.push(t);
                       }
                    }
                    break;
            }   
        }
        
        else if (t.isProcedure())
        {
            debugPrint("Evaluating procedure \'" + t.toString() + "\'.");
            //When in doubt push our procedure into our stack
            st.push(t);
        }
        
        else if (t.isBoolean())
        {
            debugPrint("Evaluating and pushing boolean \'" + t.toString() + "\'.");
            st.push(t); 
        }
        
        else if (t.isNumber())
        {
            debugPrint("Evaluating and pushing number \'" + t.toString() + "\'.");
            st.push(t);
        }
        else
        {
            System.out.println("Interpreter Error: Can't handle token \'" + t.toString() + "\'");
        }       
    }
    
    /**
     * pop
     * Pops top token
     * @post returns top Token
     */
    public Token pop()
    {
        if(!st.empty())
        {
            debugPrint("pop() : " + st.peek().toString());
            Token t = st.pop();
            return t;
        }
        else
        {
            System.out.println("interpreter error: no values to pop, returning null");
            return null;
        }
    }
    
    /*
     * debugPrint
     * Print
     * @param  s   string to print
     */
    private void debugPrint(String s)
    {
       if(debugMode)
       {
           System.out.println(s);
       }
    }
    
    /*
     * cloneToken
     * Ensures we're not holding on to referenced tokens
     * @param  t   token to clone
     * @post a cloned token
     */
    private Token cloneToken(Token t)
    {
        Token copy;
        
        if (t.isNumber()){ copy = new Token(t.getNumber());}
        else if (t.isBoolean()){ copy = new Token(t.getBoolean());}
        else if (t.isSymbol()){ copy = new Token(t.getSymbol());}
        //T is procedure
        else
        { copy = new Token(t.getProcedure());}  
        return t;
    }
    
    /*
     * pushProcedure
     * Utility pushes procedures into internal fields 
     * @param  t  of type Procedure else it fails
     * @post t is pushed into stack
     */    
    private void pushProcedure(Token t)
    { 
        if (t.isProcedure())
        {
            int count = 1;
            for (Iterator<Token> i = t.getProcedure().iterator(); i.hasNext();)
            {
                Token it = i.next();
                debugPrint(count + ":: pushing token into stack\'" + it.toString() + "\'.");
                st.push(it);
                count++;
            } 
        }
        else
        {
            System.out.println("Warning: pushProcedure() detected non procedure token : " + t.toString());
        }
    }
    
    /**
     * Main Method
     * 1) Create an Interpreter object 
     * 2) Processes/Parses the PostScript 
     *    program presented at the command line
     * 3) "debugging mode" facilitates debugging and testing. 
     *    uses word "debug" command-line parameters, 
     *    prints out informative messages about 
     *    what it is doing as it processes the PostScript tokens.
     */
    public static void main(String args[])
    {
        boolean enableDebug = false;
        Interpreter calc;
        Reader r = new Reader();
        Token t;
        
        if(args.length >0)
        {
            if(args[0].equals("debug"))
            {
                enableDebug = true;
            }
        }
        
        if (enableDebug)
        {
            calc = new Interpreter(true);
        }
        else
        {
            calc = new Interpreter();
        }
        
        while (r.hasNext())
        {
            t = (Token) r.next();
            if (t.isSymbol() && t.getSymbol().equals("quit"))
            {
                break;
            }
            else
            {
                calc.interpret(t);
            }   
        }
    }
}
