import java.util.Random;
import java.util.Scanner;

/**
 * Data Structures Lab: Day of the week Calculator
 * 
 * The DayOfTheWeek class computes any date between 01.01.1900 and 12.21.2099 and 
 * returns the correct day of the week for that date. 
 * 
 * @author Jorge Diaz
 * @version 1.0
 */
public class DayOfTheWeek
{
    // instance vars
    protected int month, day, year, weekday;
    static Scanner scanner = new Scanner( System.in );
    private static final int NUMBER_OF_GUESSES = 10;
    
    //Default constructor
    public DayOfTheWeek()
    {
        randomizeDate(false);
    }
    
    /*
     * Constructor with debug messages and error test
     */
    public DayOfTheWeek(boolean verbose)
    {
        randomizeDate(verbose);
    }
    
    //Overloaded constructor used for error tests
    public DayOfTheWeek( int month, int day, int year)
    {
      if (validateDate(day, month, year) == true)
      { 
        this.month = month;
        this.day = day;
        this.year = year;
        this.weekday = calculateWeekday(true);
        
        System.out.println("Explicit date is: " + DayOfTheWeek.convertWeekday(this.weekday)
                            + " , " + this.month + "/" + this.day +"/" + this.year);
      }
      else
      {
          System.out.println ("Warning detecting invalid date parameters: "
                            + " , " + month + "/" + day + "/" + year);
          System.out.println ("A random date will be generated");
          randomizeDate(true);
      }   
    }
    
    public int getWeekday()
    {
        return this.weekday;
    }
    
    /**
     * Generates random date value between 1900 and 2099
     */
    public void randomizeDate(boolean print)
    {
        Random r = new Random();
        this.year = (Math.abs(r.nextInt()) % 199) + 1900;
        this.month = (Math.abs(r.nextInt()) % 12) + 1;
        
        switch(this.month)
        {
            case 1: 
                this.day = (Math.abs(r.nextInt()) % 31) + 1;
                if (isLeapYear(this.year))
                    this.day = this.day - 1;
                break;
            case 3: case 5:
            case 7: case 8: case 10:
            case 12:
                this.day = (Math.abs(r.nextInt()) % 31) + 1;
                break;
            case 4: case 6:
            case 9: case 11:
                this.day = (Math.abs(r.nextInt()) % 30) + 1;
                break;
            case 2:
                this.day = (Math.abs(r.nextInt()) % 29) + 1;
                if (isLeapYear(this.year))
                    this.day = this.day - 1;
                break;
            default:
                System.out.println("Invalid month.");
                break;
        }
        
        this.weekday = calculateWeekday(false);
        
        if (print)
        {
            System.out.println("Random date is: " + this.weekday
                            + " , " + this.month + "/" + this.day +"/" + this.year); 
        }
    }
    /*
     * Ensures date between 1900 and 2099, valid months, days etc.
     */
    public boolean validateDate(int day, int month, int year)
    {
        boolean validation = true;
        
        if (year > 2099 || year < 1900)
            validation = false;
        if (month > 12 || month < 1)
            validation = false;
        
        switch(month)
        {
            case 1: case 3: case 5:
            case 7: case 8: case 10:
            case 12:
                if(day > 31 || day < 1) validation = false;
                break;
            case 4: case 6:
            case 9: case 11:
                if(day > 30 || day < 1) validation = false;
                break;
            case 2:
                if(day < 1) validation = false;
                if (isLeapYear(year))
                {
                    if (day > 29) validation = false;
                }   
                else
                {
                    if(day > 28) validation = false;
                }    
                break;
            default:
                break;
        }
        
        return validation;
    }
    
    public String toString()
    {
        return "Date is: " + this.weekday + ":" + this.month + ":" + this.day +":" + this.year ;
    }
    
    public String printDateWithoutWeekday()
    {
        return "The date is: " + this.month + ":" + this.day +":" + this.year ;
    }
    
    public boolean isLeapYear(int year)
    {
        if (((year % 4 == 0) && !(year % 100 == 0)) || (year % 400 == 0))
            return true;
        
        else return false;
    }
    /*
     * Returns the day of the week based on values inside Date class
     * Saturday = 0 , Sunday = 1, etc.
     */
    public int calculateWeekday(boolean verbose)
    {
        //Store modulus values
        int mod7Day, mod7Year, mod4Year, adjustment;
        
        mod7Day = this.day % 7;
        mod7Year = (this.year - 1900) % 7; 
        mod4Year = (this.year - 1900)/4;
        adjustment = adjustMonth(this.month, this.year);
        
        if (verbose)
        {
            System.out.format( "calculateWeekday function vars = mod7Day %d, mod7Year %d, mod4Year %d, adjustment %d\n",
                                mod7Day, mod7Year, mod4Year, adjustment);
        }
        
       return (int)(mod7Day + mod7Year + mod4Year + adjustment) % 7; 
    }
    /*
     * Translates week integer to string value
     */
    public static String convertWeekday(int num)
    {
        String day;
        
        switch (num)
        {
            case 0: day = "Saturday"; break;
            case 1: day = "Sunday"; break;
            case 2: day = "Monday"; break;
            case 3: day = "Tuesday"; break;
            case 4: day = "Wednesday"; break;
            case 5: day = "Thursday"; break;
            case 6: day = "Friday"; break;
            default: day = "Error: Weekday conversion";
        }
        
        return day;
    }
    
    /*
     * Looks up a per month adjustment value for use in our algorythm
     */
    public int adjustMonth(int month, int year)
    {
        int adjustment = -1;
        //Montly adjustment based on Bailey's algorythm table
        switch (month)
        {   
            case 4: case 7:
                adjustment = 0; 
                break;
            case 1: 
                if(isLeapYear(year))adjustment = 0; 
                else adjustment = 1;
                break;            
            case 10: 
                adjustment = 1; 
                break;
            case 5:
                adjustment = 2; 
                break;
            case 8:
                adjustment = 3; 
                break;
            case 2: 
                if (isLeapYear(year))adjustment = 3; 
                else adjustment = 4;
                break;
            case 3: case 11:
                adjustment = 4; 
                break;
            case 6:
                adjustment = 5; 
                break;
            case 9:
                adjustment = 6; 
                break;
            default:
                System.out.println("Warning: Cannot adjust month: " + month + " , adjustment = -1");
                break;
        }
        
        return adjustment; 
    }
    
    /*
     * Error diagnose date class
     */
    public static void validateDateClass()
    {
        //Prints random date
        DayOfTheWeek date1 = new DayOfTheWeek(true); //Default constructor
        DayOfTheWeek date2 = new DayOfTheWeek( 2, 7,1976); //Valid Date
        DayOfTheWeek date3 = new DayOfTheWeek( 2, 7,3000); //Invalid year
        DayOfTheWeek date4 = new DayOfTheWeek(13, 7,1976); //Invalid month
        DayOfTheWeek date5 = new DayOfTheWeek( 2,30,1976); //Invalid day
        DayOfTheWeek date6 = new DayOfTheWeek( 2,29,1904); //Monday
        DayOfTheWeek date7 = new DayOfTheWeek( 9,02,2013); //Monday 
    }
    
    private static boolean isInteger(String s)
    {
        try
        {
            Integer i = Integer.parseInt(s);
            return true;
        }
        
        catch(NumberFormatException e)
        {
            return false;
        }
    }
    
    public static void main()
    {
        int questionNumber = 1;
        int questionAnswer = -1;
        String scanInput;
        long start = System.currentTimeMillis();
        long current;
        DayOfTheWeek randDate;
        
        System.out.println("Using a number from 0-6, enter the weekday for the date listed?");
        System.out.println("NOTE: 0 = Saturday, 1 = SUNDAY etc. ");
        
        do
        {   //Create a random date
            randDate = new DayOfTheWeek();
            System.out.format("\n\nQuestion %d: What is the weekday for this day?\n", questionNumber);
            System.out.println(randDate.printDateWithoutWeekday());
            scanInput = scanner.next();
            //Read predicted day of the week
            if (DayOfTheWeek.isInteger(scanInput) && (Integer.parseInt(scanInput) > 0 && Integer.parseInt(scanInput) < 7) )
            {
                questionAnswer = Integer.parseInt(scanInput);
                
                //Check correctness of guess 
               System.out.format("At %d seconds your entry was %d or %s.", 
                                current = System.currentTimeMillis()-start,
                                questionAnswer,
                                DayOfTheWeek.convertWeekday(questionAnswer));
                                    
                System.out.format("%n%nThe correct answer was... %d or %s.%n", 
                                randDate.getWeekday(),
                                DayOfTheWeek.convertWeekday(randDate.getWeekday())); 
                
                
                if (questionAnswer == randDate.getWeekday())
                {
                    System.out.println("Congratulations!!!");
                }
                else System.out.println("Sorry wrong answer");
            
                System.out.format("%n%n%d of %d Questions Answered%n", questionNumber, NUMBER_OF_GUESSES );
                questionNumber++;
            }
            
            //Handle inputs
            else
            {
               questionAnswer = -1;
               System.out.println("Error detected only enter numbers from 0-6.");
            }
            
        }
        //Stop when X dates are guessed correctly
        while (questionNumber <= NUMBER_OF_GUESSES);
        
        long duration = System.currentTimeMillis()-start;
        System.out.println("Final Time: "+(duration/1000.0)+" seconds.");

    }
}